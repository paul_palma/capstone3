import { useState } from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';

import UserContext from './UserContext';

import AppNavbar from './components/AppNavbar';
// import Product from './components/Product';

import Products from './pages/Products';
import Home from './pages/Home';
import Login from './pages/Login';
import NotFound from './pages/NotFound';
import Register from './pages/Register';



export default function App() {

  const [user, setUser]= useState({
    token: localStorage.getItem('token')
  });

  const unsetUser = () => {
    localStorage.clear();
    setUser({token: null});
  }


    return (
      <UserContext.Provider value={{ user, setUser, unsetUser }}>
        <Router>
            <AppNavbar/>
            <Routes>
            <Route exact path="/products" element={<Products/>}/>
                <Route exact path="/" element={<Home/>}/>
                
                <Route exact path="/login" element={<Login/>}/>
                <Route exact path="/register" element={<Register/>}/>
                <Route path="*" element={<NotFound/>}/>
            </Routes>
        </Router>
       </UserContext.Provider> 
    );
}
