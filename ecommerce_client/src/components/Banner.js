
import Jumbotron from 'react-bootstrap/Jumbotron'
import Button from 'react-bootstrap/Button'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
// import div from 'bootstrap/div'

export default function Banner(){
	return(
		<Row>
			<Col xs={12} md={4} className="mx-auto my-3">
					<Jumbotron>
						<h1>Gendry Baratheon</h1>
						<p>The mastersmith of Kings Landing</p>
					</Jumbotron>
			</Col>
		</Row>
	)
}