import { useState, useEffect } from 'react';

import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';



export default function Product({ productProp }) {


	const [count, setCount] = useState(0)
	const [seats, setSeats] = useState(10)
	const [isOpen, setIsOpen] = useState(true)
	const { name, description, price } = productProp

	useEffect(() => {
		if (seats === 0){
			setIsOpen(false)
		}
	}, []) 
	const enrol = () => {
		setCount(count + 1)
		setSeats(seats - 1)
	}

	return(
		<Card>
			<Card.Body>
				<Card.Title>{name}</Card.Title>
				<Card.Text>
					<h3>Description:</h3>
					<p>{description}</p>

					<h4>Price:</h4>
					<p>PHP {price}</p>	

					<h4>Enrollees</h4>
					<p>{count}</p>

					<h4>Seats</h4>	
					<p>{seats}</p>
				</Card.Text>
				<Button variant="primary" onClick={enrol} disabled={!isOpen}>Enroll</Button>
			</Card.Body>
		</Card>
	)
}