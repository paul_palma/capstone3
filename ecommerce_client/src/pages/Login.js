
import { useState, useEffect } from 'react';
import { Container, Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function Login() {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isLoginDisabled, setIsLoginDisabled] = useState(false);

    useEffect(() => {
        if (email === '' || password === '') {
            setIsLoginDisabled(true);
        } else {
            setIsLoginDisabled(false);
        }

    }, [email, password]);

    const login = async (e) => {
        e.preventDefault();

        const payload = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ email, password })
        };

        const response = await fetch('http://localhost:4000/users/login', payload);
        const data = await response.json();

        if (typeof data.accessToken !== 'undefined') {
            localStorage.setItem('token', data.accessToken);
            getUserData(data.accessToken);
        } else {
            Swal.fire('Login Failed', 'Credentials not found, try again.', 'error');
        }
    }

    const getUserData = async (token) => {
        const payload = {
            headers: {
                'Authorization': `Bearer ${token}`
            }
        };

        const response = await fetch('http://localhost:4000/users/details', payload);
        const data = await response.json();

        localStorage.setItem('id', data._id);
        localStorage.setItem('isAdmin', data.isAdmin);
    }

    return (
        <Container>
            <h3>Login</h3>
            <Form className="mt-3" onSubmit={login}>
                <Form.Group>
                    <Form.Label>Email</Form.Label>
                    <Form.Control type="text" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)} required/>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Enter password" value={password} onChange={e => setPassword(e.target.value)} required/>
                </Form.Group>
                <Button type="submit" variant="success" disabled={isLoginDisabled}>Login</Button>
            </Form>
        </Container>
    );
}
