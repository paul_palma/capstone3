import { useState, useEffect } from 'react';
import Product from "../components/Products";

// import productData from "../data/products";

export default function Products(){

	const [productData, setProductsData] = useState([])

	//Run one fetch request ON COMPONENT MOUNT (when the component loads for the first time)

	//When a component mounts, all states are assigned their default values (this is considered a state change even if the component has no states)

	//Always start writing useEffect like this
	// useEffect(() => {

	// }, [])

	useEffect(() => {

		fetch('http://localhost:4000/products')
		.then(res => res.json())
		.then(data => {
			// console.log(data)
			setProductsData(data)
		})

	}, [])

	//useEffect is a React hook that allows any code inside of its scope to happen when a component mounts (if array is empty) OR when a state inside its array changes (can have any number of states in array)

	//By putting our fetch request inside of a useEffect, it will only fetch our data on initial load/component mount (and not infinitely)

	const products = productsData.map(product => {
		return(
			<product key={product._id} productProp={product}/>
		)
	})

	return(
		<>
			{product}
		</>
	)
}