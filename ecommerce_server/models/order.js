const mongoose = require("mongoose");



const orderSchema = new mongoose.Schema({
	userId: String,
	products: [
		{
			productId: String, 
			price: Number,
			quantity: Number 
		}
	],
	totalAmount: Number,
	purchasedOn: {
		type: Date,
		default: new Date() 
	}			 
})

module.exports = mongoose.model ("Order", orderSchema)